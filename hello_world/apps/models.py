from django.db import models

class Table_1(models.Model):
    table_1_id = models.IntegerField()
    field_1 = models.CharField(max_length=200)

class Table_2(models.Model):
    table_2_id = models.IntegerField()
    field_2 = models.CharField(max_length=200)
    table_1_id = models.ForeignKey(Table_1, on_delete=models.CASCADE)
