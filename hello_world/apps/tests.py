from django.test import TestCase
from .models import Table_1, Table_2

class hello_worldTests(TestCase):

    def test_connection_table1_table2(self):
        t1 = Table_1(table_1_id=1, field_1='Header')
        t1.save()
        t2 = Table_2(table_2_id=2, field_2='Some text_1', table_1_id=t1)
        t2.save()
        t3 = Table_2(table_2_id=3, field_2='Some text_2', table_1_id=t1)
        t3.save()
        self.assertIs(t2.table_1_id==t1, True)
        self.assertIs(t3.table_1_id==t1, True)
        self.assertIs(len(Table_2.objects.filter(table_1_id=t1))==2, True)
