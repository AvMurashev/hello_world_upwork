from django.shortcuts import render
from apps.models import Table_1, Table_2


def hello_world(request):
    table_1 = Table_1.objects.all()[0]  # Get one (first) table_1 element of class Table_1
    table_2s = Table_2.objects.filter(table_1_id=table_1)  # Get all elements of class Table_2 with related entries from Table_1
    return render(request, 'apps/hello_world.html', locals())
